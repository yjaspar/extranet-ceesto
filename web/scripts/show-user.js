$(document).ready(function(){
	if ($('#code').length){
	    var editor = CodeMirror.fromTextArea(document.getElementById("code"), {
		    mode: {name: "xml", alignCDATA: true},
		    lineNumbers: true,
		    theme: 'monokai',
		    onKeyEvent: function(editor, event){
			if (event.keyIdentifier == 'Control' && event.type == 'keyup')
			    $('#display-preview').html(editor.doc.getValue().replace('{{title}}', $('#name').attr('value')).replace('{{description}}', $('#description').val()));
		    }
		});
	    var choice = document.location.search &&
		decodeURIComponent(document.location.search.slice(1));
	}

	$('#display-add').click(function(){
		if($('.bg-forms').is(':hidden')){
			$('.bg-forms').slideDown('fast');
		}else{
			$('.bg-forms').slideUp('fast');
		}
	});
});