<?php
 
namespace Extranet\DashboardBundle\Twig\Extension;
 
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Bundle\TwigBundle\Loader\FilesystemLoader;
 
class Md5Extension extends \Twig_Extension
{
    public function getFilters()
    {
        return array(
            'md5' => new \Twig_Filter_Method($this, 'md5'),
        );
    }

    public function md5($text)
    {
        return md5($text);
    }

    public function getName()
    {
        return 'Md5Extension';
    }
}
