<?php
namespace Extranet\DashboardBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class AddUser extends AbstractType
{
    /**
     * Builds the AddUser form
     * @param  \Symfony\Component\Form\FormBuilder $builder
     * @param  array $options
     * @return void
     */
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder->add('username');
        $builder->add('email');
        $builder->add('password');
        $builder->add('roles');
        $builder->add('plugins');
    }

    /**
     * Returns the default options for this form type.
     * @param array $options
     * @return array The default options
     */
    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'Extranet\DashboardBundle\Document\User'
        );
    }

    /**
     * Gets the unique name of this form type
     * @return string
     */
    public function getName()
    {
        return 'add_user';
    }
}