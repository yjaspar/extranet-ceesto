<?php

namespace Extranet\DashboardBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;
use FOS\UserBundle\Form\Type\LoginFormType as BaseType;

class LoginFormType extends BaseType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        // add your custom field
        $builder->add('Username');
    }

    public function getName()
    {
        return 'acme_user_registration';
    }
}