<?php
namespace Extranet\DashboardBundle\Document;

use FOS\UserBundle\Document\Plugin as BaseGroup;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document
 */
class Plugin
{
    /**
     * @MongoDB\Id(strategy="auto")
     */
    private $id;

    /** @MongoDB\Field(type="string") */
    private $name;

    /** @MongoDB\Field(type="string") */
    private $icone;

    /** @MongoDB\Field(type="string") */
    private $style;

    /** @MongoDB\Field(type="string") */
    private $iconeBinary;

    /** @MongoDB\Field(type="string") */
    private $description;

    /** @MongoDB\Field(type="string") */
    private $slug;

    /** @MongoDB\Field(type="string") */
    private $updated_at;

    /**
     * @MongoDB\Date
     */
    private $created_at;
    

    public function getId()
    {
        return $this->id;
    }

    public function setIconeBinary($value)
    {
        $this->iconeBinary = $value;
    
        return $this;
    }

    public function getIconeBinary()
    {
        return $this->iconeBinary;
    }

    public function setStyle($value)
    {
        $this->style = $value;
    
        return $this;
    }

    public function getStyle()
    {
        return html_entity_decode($this->style);
    }

    public function setDescription($value)
    {
        $this->description = $value;
    
        return $this;
    }

    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Plugin
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set icone
     *
     * @param string $icone
     * @return Plugin
     */
    public function setIcone($icone)
    {
        $this->icone = $icone;
    
        return $this;
    }

    /**
     * Get icone
     *
     * @return string 
     */
    public function getIcone()
    {
        return $this->icone;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Plugin
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    
        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set updated_at
     *
     * @param \DateTime $updatedAt
     * @return Plugin
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;
    
        return $this;
    }

    /**
     * Get updated_at
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return Plugin
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;
    
        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }
}
