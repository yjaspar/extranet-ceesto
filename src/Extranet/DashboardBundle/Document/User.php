<?php
namespace Extranet\DashboardBundle\Document;

use FOS\UserBundle\Document\User as BaseUser;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document
 */
class User extends BaseUser
{
	/**
     * @MongoDB\Id(strategy="auto")
     */
    protected $id;

    /**
     * @MongoDB\ReferenceMany(targetDocument="Extranet\DashboardBundle\Document\Group")
     */
    protected $groups;

    /** @MongoDB\Field(type="boolean") */
    protected $enabled;

    /**
     * @MongoDB\ReferenceMany(targetDocument="Extranet\DashboardBundle\Document\Plugin")
     */
    private $plugins;

    public function __construct()
    {
        parent::__construct();
        $this->plugins = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    public function getPlugins()
    {
        return ($this->plugins);
    }
    
    /**
    * @param Plugin $plugin
    */
    public function addPlugin(Plugin $plugin){
	    $this->plugins[] = $plugin;
    }

    public function setEnabled($value){
        $this->enabled = $value;
    }

    public function clearPlugins(){
        $this->plugins = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getRole(){
        return ($this->roles);
    }
}