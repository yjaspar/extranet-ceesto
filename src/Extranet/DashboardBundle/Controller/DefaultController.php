<?php

namespace Extranet\DashboardBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
	
    public function indexAction()
    {
    	$securityContext = $this->container->get('security.context');
    	if ($securityContext->isGranted('ROLE_ADMIN') ||
    		$securityContext->isGranted('ROLE_USER')){
			
    		$session = new Session();
    		$user = $this->container->get('security.context')->getToken()->getUser();
    		$plugins = $user->getPlugins()->toArray();
    		
			return $this->render('ExtranetDashboardBundle:Default:index.html.twig', array('self' => $this->get('security.context')->getToken()->getUser(), 'plugins'=>$plugins, 'admin' => ($securityContext->isGranted('ROLE_ADMIN') || $securityContext->isGranted('ROLE_SUPER_ADMIN')?true:false), 'title' => 'Dashboard'));
    	}else{
			return $this->redirect($this->generateUrl('login'));
		}
    }

    public function showUsersAction(){
		if ($this->get('security.context')->isGranted('ROLE_SUPER_ADMIN') ||
			$this->get('security.context')->isGranted('ROLE_ADMIN')){
			$dm = $this->get('doctrine_mongodb')->getManager();
			if ($this->get('security.context')->isGranted('ROLE_SUPER_ADMIN')){
				$users = $dm->getRepository('ExtranetDashboardBundle:User')->findAll();
			}else{
				$qb = $dm->createQueryBuilder('ExtranetDashboardBundle:User');
		        $qb->field('roles.0')->notEqual('ROLE_SUPER_ADMIN');
		        $users = $qb->getQuery()->execute();
			}
			$plugins = $dm->getRepository('ExtranetDashboardBundle:Plugin')->findAll();

			return $this->render('ExtranetDashboardBundle:Default:show.html.twig', array('self' => $this->get('security.context')->getToken()->getUser(), 'users'=>$users, 'plugins'=>$plugins, 'admin' => $this->get('security.context')->isGranted('ROLE_SUPER_ADMIN'), 'title' => 'User manager'));
		}else{
			return $this->redirect($this->generateUrl('_welcome'));
		}
    }

    public function addUserAction(){
		if ($this->get('security.context')->isGranted('ROLE_SUPER_ADMIN') ||
		$this->get('security.context')->isGranted('ROLE_ADMIN')){
		    $dm = $this->get('doctrine_mongodb')->getManager();
		    $user = new \Extranet\DashboardBundle\Document\User();
		    
		    $user->setUsername($_POST['email']);
		    $user->setEmail($_POST['email']);

		    $factory = $this->get('security.encoder_factory');
			
			$encoder = $factory->getEncoder($user);
			$password = $encoder->encodePassword($_POST['password'], $user->getSalt());
			
			$user->setPassword($password);
		    $user->setRoles(array($_POST['role']));

		    $user->setEnabled(true);

		    foreach($_POST['plugins'] as $plg)
		    	$user->addPlugin($dm->getRepository('ExtranetDashboardBundle:Plugin')->findOneById($plg));
		    
		    $dm->persist($user);
		    $dm->flush();
		    
		    return $this->redirect($this->generateUrl('showUsers'));
		}else{
			return $this->redirect($this->generateUrl('_welcome'));
		}
    }

    public function addPluginAction(){
	    if ($this->get('security.context')->isGranted('ROLE_SUPER_ADMIN')){
		    $dm = $this->get('doctrine_mongodb')->getManager();
		    $plugin = new \Extranet\DashboardBundle\Document\Plugin();
		    
		    // Mise en forme de la bulle du dashboard
			$style=$_POST['style'];
			$style=preg_replace('/{{title}}/', $_POST['name'], $style);
			$style=preg_replace('/{{description}}/', $_POST['description'], $style);

			$plugin->setName($_POST['name']);
			$plugin->setSlug($_POST['slug']);
			$plugin->setStyle($style);
			$plugin->setDescription($_POST['description']);

		    $dm->persist($plugin);
		    $dm->flush();
		}
	    return $this->redirect($this->generateUrl('showUsers'));
    }

	public function deletePluginAction($id){
		if ($this->get('security.context')->isGranted('ROLE_SUPER_ADMIN')){
			$dm = $this->get('doctrine_mongodb')->getManager();
			$qb = $dm->createQueryBuilder('ExtranetDashboardBundle:User');
	        $qb->expr()->field('plugins')->elemMatch(
	            $qb->expr()->field('$id')->equals(new \MongoId($id))
	        );
	        $users = $qb->getQuery()->execute();
	        foreach($users as $user){
	        	foreach($user->getPlugins() as $pop => $plugin){
	        		if ($plugin->getId() == $id){
	        			echo 'User: '.$user->getEmail().' - POP: '.$pop;
	        			$dm->createQueryBuilder('ExtranetDashboardBundle:User')
					    ->update()
					    ->field('plugins.'.$pop)->unsetField()->exists(true)
					    ->getQuery()
					    ->execute();
	        		}
	        	}
	        }

			$plugin = $dm->getRepository('ExtranetDashboardBundle:Plugin')
	            ->find($id);
	        $dm->remove($plugin);
			$dm->flush();
		}
	    return $this->redirect($this->generateUrl('showUsers'));
    }

	public function editPluginAction($id){
		if ($this->get('security.context')->isGranted('ROLE_SUPER_ADMIN')){
			$dm = $this->get('doctrine_mongodb')->getManager();
			$plugin = $dm->getRepository('ExtranetDashboardBundle:Plugin')
			            ->find($id);
			if ($_POST){
				// Mise en forme de la bulle du dashboard
				$style=$_POST['style'];
				$style=preg_replace('/{{title}}/', $_POST['name'], $style);
				$style=preg_replace('/{{description}}/', $_POST['description'], $style);

				$plugin->setName($_POST['name']);
				$plugin->setSlug($_POST['slug']);
				$plugin->setStyle($style);
				$plugin->setDescription($_POST['description']);

				$dm->flush();

				return $this->redirect($this->generateUrl('showUsers'));
			}
		
		
			$style = str_replace($plugin->getDescription(), '{{description}}', $plugin->getStyle());
			$style = str_replace($plugin->getName(), '{{title}}', $style);

			$plugin_ = array(
				'id' => $plugin->getId(),
				'title' => $plugin->getName(),
				'slug' => $plugin->getSlug(),
				'description' => $plugin->getDescription(),
				'style' => $style
			);

			return $this->render('ExtranetDashboardBundle:Default:editPlugin.html.twig', array('self' => $this->get('security.context')->getToken()->getUser(), 'plugin'=>$plugin_, 'title' => 'User manager'));
		}else{
			return $this->redirect($this->generateUrl('showUsers'));
		}
	}

    public function deleteUserAction($id){
		if ($this->get('security.context')->isGranted('ROLE_SUPER_ADMIN') ||
			$this->get('security.context')->isGranted('ROLE_ADMIN')){
			
			$dm = $this->get('doctrine_mongodb')->getManager();
			$user = $dm->getRepository('ExtranetDashboardBundle:User')
	            ->find($id);
	        if ($this->get('security.context')->isGranted('ROLE_SUPER_ADMIN') || 
	        	($this->get('security.context')->isGranted('ROLE_ADMIN') &&
	        	!in_array('ROLE_SUPER_ADMIN', $user->getRole()))){
	        	$dm->remove($user);
	        }
			$dm->flush();
		}
	    return $this->redirect($this->generateUrl('showUsers'));
    }

    public function editUserAction($id){
		if ($this->get('security.context')->isGranted('ROLE_SUPER_ADMIN') ||
			$this->get('security.context')->isGranted('ROLE_ADMIN') ||
			$this->get('security.context')->getToken()->getUser()->getId() == $id){

			$dm = $this->get('doctrine_mongodb')->getManager();
			$user = $dm->getRepository('ExtranetDashboardBundle:User')
			            ->find($id);
			if ($_POST){
				$user->setUsername($_POST['email']);
				$user->setEmail($_POST['email']);
				$user->setRoles(array($_POST['role']));

				if ($_POST['password']!=""){
					$factory = $this->get('security.encoder_factory');
					
					$encoder = $factory->getEncoder($user);
					$password = $encoder->encodePassword($_POST['password'], $user->getSalt());

					$user->setPassword($password);
				}
				$user->setRoles(array($_POST['role']));

				$user->clearPlugins();

				if (isset($_POST['plugins'])){
					foreach($_POST['plugins'] as $k => $v){
						if (!$this->customInArray($v, $user->getPlugins()->toArray(), 'id'))
							$user->addPlugin($dm->getRepository('ExtranetDashboardBundle:Plugin')->find($v));
					}
				}
				$dm->flush();
				return $this->redirect($this->generateUrl('showUsers'));
			}
			
			$plugins = $dm->getRepository('ExtranetDashboardBundle:Plugin')
	            ->findAll();
			
			$plg = '<select name="plugins[]" multiple>';

			foreach($plugins as $p){
				if ($this->customInArray($p->getName(), $user->getPlugins()->toArray(), 'name')){
					$plg .= '<option selected value="'.$p->getId().'">'.$p->getName().'</option>';
				}else{
					$plg .= '<option value="'.$p->getId().'">'.$p->getName().'</option>';	
				}
			}

			$plg .= '</select><br />';

			return $this->render('ExtranetDashboardBundle:Default:edit.html.twig', array('self' => $this->get('security.context')->getToken()->getUser(), 'users'=>$user, 'super_admin' =>  $this->get('security.context')->isGranted('ROLE_SUPER_ADMIN'), 'plugins_form' => $plg, 'title' => 'User manager'));
		}else{
			return $this->redirect($this->generateUrl('_welcome'));
		}
    }

    public function displayImageAction($id){
        $dm = $this->get('doctrine_mongodb')->getManager();

        $img = $dm->getRepository('ExtranetDashboardBundle:Upload')
                    ->find($id);

        if (null === $img) {
        	throw $this->createNotFoundException(sprintf('Upload with id "%s" could not be found', $id));
        }

        $response = new Response();

        $response->headers->set('Content-Type', $img->getMimeType());
        $response->headers->set('Content-disposition', 'attachment;filename="'.$img->getFileName().'"');
     
        $response->setContent($img->getFile()->getBytes());
     
        return $response;
    } 

    private function customInArray($match, $array, $type){
	    foreach($array as $k => $v){
		    switch ($type) {
		    	case 'name':
		    		if ($v->getName() === $match)
		    			return (true);
		    		break;
		    	case 'id':
		    		if ($v->getId() === $match)
		    			return (true);
		    		break;
		    }
	    }
	    return (false);
    }
}
